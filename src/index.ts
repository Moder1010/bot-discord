import { Client, Collection, GatewayIntentBits, Guild, TextChannel, Events } from 'discord.js'
import { newCorrect } from "./gSheets"
import { createIfNotExists } from "./dbutils"
import { setupCommandHandeling } from './commandHandeling'
require('dotenv').config()
let client = new Client(
	{
		intents: [GatewayIntentBits.GuildMembers, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent, GatewayIntentBits.Guilds, GatewayIntentBits.GuildIntegrations]
	}) as Client & { commands: Collection<string, any> }
client.commands = new Collection();

let guild: Guild

let roles = ["Comm/Event", "Chef de Projet", "Prospection", "RH", "Président", "Vice-président(e) interne", "Vice-président(e) externe", "Sec-Gen", "Trésorier", "Vice-trésorier"]


client.on("ready", async () => {
	await createIfNotExists() //db
	guild = await client.guilds.fetch(process.env.GUILD_ID as string);
	guild.channels.fetch()
	guild.roles.fetch()
	setupCommandHandeling(client)
	console.log("ready");
});


client.on("messageCreate", async (msg) => {
	if (!msg.reference || !msg.reference.messageId) return
	let original = await msg.channel.messages.fetch(msg.reference.messageId)
	if (!msg.member || !original.member) return
	guild.members.fetch(original.member)
	guild.members.fetch(msg.member.id)

	if (!msg.content.startsWith("corrige") &&
		!msg.content.startsWith("corrigé") &&
		!msg.content.startsWith("corr")) return
	if (!msg.member?.roles.cache.has(process.env.QUALI_ROLE_ID as string)) return

	let contenu = msg.content.replace(/(corr|corrig(e|é)) (.*)/, "$3")

	let pole = roles.find(r => original.member?.roles.cache.find(role => role.name == r))
	if (pole == undefined) pole = "inconnu"
	if (pole == "Vice-président(e) interne" || pole == "Vice-président(e) externe")
		pole = "VP"
	if (pole == "Trésorier" || pole == "Vice-trésorier")
		pole = "Trésorerie"

	await newCorrect(original.createdTimestamp, msg.createdTimestamp, contenu, pole, original.member?.displayName, msg.member.displayName)
		.catch((err: { msg: string, err: any }) => {
			console.error(err)
			if (err.err)
				msg.reply(err.msg + "\n" + err.err.toString())
			else
				msg.reply(err.msg)
		})
	msg.react("✅")
})

client.login(process.env.DISCORD_TOKEN)