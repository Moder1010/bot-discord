import { SlashCommandBuilder, ChatInputCommandInteraction} from 'discord.js'
import { updateValue, getValue } from "../dbutils"
import { getsheetName } from '../gSheets';

module.exports = {
	data: new SlashCommandBuilder()
		.setName('change-sheet')
		.setDescription('change l\'ID du GSheet')
		.addStringOption(option =>
			option.setName('sheet-id')
				.setDescription('l\'ID du GSheet (https://docs.google.com/spreadsheets/d/<ID DU GSHEET>/edit...)')
				.setRequired(false)),
	async execute(interaction: ChatInputCommandInteraction) {
		if(interaction.options.getString("sheet-id"))
			await updateValue("sheetID", interaction.options.getString("sheet-id") as string)
		let sheetID = await getValue("sheetID").catch(()=>{return undefined})
		let sheetName = await getsheetName().catch(()=>{return undefined})
		await interaction.reply(`Done.\nsheet id = ${sheetID||"NOT SET"}\nsheet name (onglet) = ${sheetName||"NOT SET"}`)
	},
};