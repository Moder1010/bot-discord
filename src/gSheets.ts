const fs = require('fs').promises;
const path = require('path');
import { authenticate } from '@google-cloud/local-auth';
import { OAuth2Client } from 'google-auth-library';
import { google } from 'googleapis';
import moment from "moment";
import momentDurationFormatSetup from "moment-duration-format"
import { getValue } from "./dbutils"

momentDurationFormatSetup(moment as any)

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];

const TOKEN_PATH = path.join(process.cwd(), 'token.json');
const CREDENTIALS_PATH = path.join(process.cwd(), 'credentials.json');

async function loadSavedCredentialsIfExist() {
    try {
        const content = await fs.readFile(TOKEN_PATH);
        const credentials = JSON.parse(content);
        return google.auth.fromJSON(credentials);
    } catch (err) {
        return null;
    }
}

async function saveCredentials(client: OAuth2Client) {
    const content = await fs.readFile(CREDENTIALS_PATH);
    const keys = JSON.parse(content);
    const key = keys.installed || keys.web;
    const payload = JSON.stringify({
        type: 'authorized_user',
        client_id: process.env.GOOGLE_CLIENT_ID,
        client_secret: process.env.GOOGLE_CLIENT_SECRET,
        refresh_token: client.credentials.refresh_token,
    });
    await fs.writeFile(TOKEN_PATH, payload);
}

async function authorize() {
    let existingClient = await loadSavedCredentialsIfExist();
    if (existingClient) {
        return existingClient;
    }
    let client = await authenticate({
        scopes: SCOPES,
        keyfilePath: CREDENTIALS_PATH,
    });
    if (client.credentials) {
        await saveCredentials(client);
    }
    return client;
}

export async function getsheetName(): Promise<string> {
    let auth = await authorize();
    const sheets = google.sheets({ version: 'v4', auth });
    let spreadSheetID = await getValue("sheetID");
    moment.locale("fr")
    let nom = moment().format("MMMM YYYY")
    nom = nom.charAt(0).toUpperCase() + nom.slice(1);


    const response = await sheets.spreadsheets.get({
        auth: auth,
        spreadsheetId: spreadSheetID,
    });

    if (!response.data.sheets) throw { msg: "erreur lors de la récupération des sheets", err: null }

    let sheetName = response.data.sheets.find(sheet => sheet.properties && sheet.properties.title === nom)?.properties?.title
    if (sheetName) return sheetName

    let sheetID = response.data.sheets.find(sheet => sheet.properties && sheet.properties.title && sheet.properties.title.match(/^(T|t)emplate$/))?.properties?.sheetId
    if (!sheetID) throw { msg: "erreur lors de la récupération du sheet template", err: null }

    const responseDuplicate = await sheets.spreadsheets.sheets.copyTo({
        auth: auth,
        spreadsheetId: spreadSheetID,
        sheetId: sheetID,
        requestBody: {
            destinationSpreadsheetId: spreadSheetID,
        }
    }).catch(err => { throw { msg: "erreur lors de la duplication du template", err: err } })

    const newSheetId = responseDuplicate.data.sheetId;
    await sheets.spreadsheets.batchUpdate({
        auth: auth,
        spreadsheetId: spreadSheetID,
        requestBody: {
            requests: [
                {
                    updateSheetProperties: {
                        properties: {
                            sheetId: newSheetId,
                            title: nom,
                        },
                        fields: 'title',
                    },
                },
            ],
        },
    }).catch(err => { throw { msg: "erreur lors du renommage du nouveau sheet", err: err } });
    return nom
}

export async function newCorrect(originalDate: number, responseDate: number, contenu: string, pole: string, demandeur: string, membreQuali: string) {
    let auth = await authorize();
    const sheets = google.sheets({ version: 'v4', auth });
    try {
        const res = await sheets.spreadsheets.values.get({
            spreadsheetId: await getValue("sheetID"),
            range: await getsheetName() + '!A:F',
        });


        let d = moment.duration(moment(responseDate).diff(moment(originalDate)))
        let duree: string

        if (d.days())
            duree = d.format("d[j] hh:mm:ss", { trim: false })
        else
            duree = d.format("hh:mm:ss", { trim: false })



        const data = res.data.values as any[][];
        let i: number;
        for (i = 7; i < data.length; i++) {
            let empty = true
            for (let j = 0; j < 5; j++)
                if (data[i][j])
                    empty = false
            if (empty) break
        }

        await sheets.spreadsheets.values.update({
            spreadsheetId: await getValue("sheetID"),
            range: await getsheetName() + `!A${i + 1}:F${i + 1}`,
            valueInputOption: "USER_ENTERED",
            requestBody: { values: [[moment(originalDate).format("DD/MM/YYYY"), duree, contenu, pole, demandeur, membreQuali]] }
        })
    } catch (err: any) {
        if (err.code == 404) {
            throw { msg: "erreur : l'id du GSheet est probablement erronée", err: err }
        }
        if (err.code == 400) {
            throw { msg: "erreur : le nom du sheet (onglet) du GSheet est probablement erroné", err: err }
        }
        throw err
    }
}