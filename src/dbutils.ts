import sqlite from "sqlite3"
require("dotenv").config()

function getDb(): sqlite.Database {
    let db = new sqlite.Database(process.env.DB_FILE_PATH as string, (err) => {
        if (!err) return
        console.error("error opening db file")
        console.error(err)
    })
    return db
}

export async function createIfNotExists() {
    let db = getDb()
    await new Promise<void>(r=>db.run('CREATE TABLE IF NOT EXISTS gsheetparams(id INTEGER PRIMARY KEY, name TEXT NOT NULL, value TEXT);',()=>r()));
    db.close()
}

export async function updateValue(valueName: string, newValue: string):Promise<void> {
    let db = getDb()
    let value = await new Promise<unknown>(r=>db.get(`SELECT value FROM gsheetparams WHERE name = "${valueName}"`, (err, row) => {
        if (err) throw err
        r(row)
    }))
    if (!value) {
        await new Promise<void>(r=>db.run(`INSERT INTO gsheetparams (name, value) VALUES("${valueName}", "${newValue}");`,()=>r()));
    } else {
        await new Promise<void>(r=>db.run(`UPDATE gsheetparams SET value="${newValue}" WHERE name="${valueName}"`, ()=>r()));
    }
    db.close()
}

export async function getValue(valueName: string): Promise<string> {
    let db = getDb()
    let value = await new Promise<unknown>(r => db.get(`SELECT value FROM gsheetparams WHERE name = "${valueName}"`, (err, row) => {
        if (err) throw err
        r(row)
    }))
    db.close()
    if (!value) throw "la donnée demandé n'est pas set (" + valueName + ")"
    return (value as {value:string}).value
}

//db.run("INSERT INTO gsheetparams (name, value) VALUES (\"sheetID\", \"1p7XxD-wGDykLLZLYDNUu_juMgtBlC8slbT4g4kWO1Fc\")")