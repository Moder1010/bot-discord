import { REST, Routes } from 'discord.js';
require("dotenv").config()

let clientId = process.env.DISCORD_CLIENT_ID as string
let guildId = process.env.GUILD_ID as string

const rest = new REST().setToken(process.env.DISCORD_TOKEN as string);

rest.delete(Routes.applicationGuildCommand(clientId, guildId, "1095408866256244786"))
	.then(() => console.log('Successfully deleted guild command'))
	.catch(console.error);